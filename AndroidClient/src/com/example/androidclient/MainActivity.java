/**
 * Deze klasse is een subklasse van de ActionBarActivity en verzorgt de hoofdtaak van de app: Het opvragen van gegevens
 * over parkings in Gent en met deze data een listview opvullen.
 *
 * @author Geert Ari�n
 * @version 14 oktober 2014
 */

package com.example.androidclient;

import android.content.Context;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends ActionBarActivity {

	/**
	 * De url waar de data wordt opgehaald.
	 */
	private static final String stringUrl = "http://datatank.gent.be/Mobiliteitsbedrijf/Parkings.json";

	/**
	 * De data wordt voor het eerst opgehaald bij het aanmaken van de app layout.
	 * 
	 * {@inheritDoc}
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		fetchData();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * Als de refresh button wordt geselecteerd wordt de data opnieuw opgehaald.
	 * @param view De view waar de button werd geselecteerd.
	 */
	public void refreshButtonClickHandler(View view) {
		findViewById(R.id.buttonRefresh).setEnabled(false);
		fetchData();
	}

	/**
	 * Ophalen van de data, eerst wordt er gecontroleerd of er een connectie is met het internet.
	 */
	private void fetchData() {
		ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
		// Before attempting to fetch the URL, makes sure that there is a
		// network connection.
		if (networkInfo != null && networkInfo.isConnected()) {
			new DownloadDataTask(this).execute(stringUrl);
		} else {
			TextView textViewStatus = (TextView) findViewById(R.id.textViewStatus);
			textViewStatus.setText("No connection available.");
			textViewStatus.setTextColor(Color.RED);
		}
	}
}
