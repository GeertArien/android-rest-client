/**
 * Deze klasse is een subklasse van de ArrayAdapter parking en zorgt voor de databinding tussen
 * een listview item en een Parking object.
 *
 * @author Geert Ari�n
 * @version 14 oktober 2014
 */

package com.example.androidclient;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class ArrayAdapterParking extends ArrayAdapter<Parking> {

	/**
	 * Het activity waarin de adapter werd aangemaakt.
	 */
	private Context mContext;

	/**
	 * The resource ID for a layout file containing a TextView to use when instantiating views.
	 */
	private int layoutResourceId;

	/**
	 * Een array van Parking objecten.
	 */
	private Parking data[] = null;

	/**
	 * Niet standaard constructor.
	 * 
	 * @param mContext Het activity waarin de adapter werd aangemaakt
	 * @param layoutResourceId The resource ID for a layout file containing a TextView to use when instantiating views.
	 * @param data Een array van Parking objecten.
	 */
	public ArrayAdapterParking(Context mContext, int layoutResourceId,
			Parking[] data) {
		super(mContext, layoutResourceId, data);

		this.layoutResourceId = layoutResourceId;
		this.mContext = mContext;
		this.data = data;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		if (convertView == null) {
			// inflate the layout
			LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
			convertView = inflater.inflate(layoutResourceId, parent, false);
		}

		// object item based on the position
		Parking parking = data[position];

		// get the TextView and then set the text
		((TextView) convertView.findViewById(R.id.parkingName)).setText(parking
				.getName());
		((TextView) convertView.findViewById(R.id.parkingCapacity))
				.setText(Integer.toString(parking.getCapacity()));
		((TextView) convertView.findViewById(R.id.parkingFree)).setText(Integer
				.toString(parking.getfreePlaces()));
		return convertView;
	}
}
