/**
 * Deze klasse is een subklasse van de AsyncTask klasse en zorgt ervoor dat de connectie met het internet
 * in een aparte thread loopt.
 *
 * @author Geert Ari�n
 * @version 14 oktober 2014
 */

package com.example.androidclient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;

import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;

public class DownloadDataTask extends AsyncTask<String, Void, Parking[]> {

	/**
	 * Debug tag.
	 */
	private static final String DEBUG_TAG = "ParkingNetwork";

	/**
	 * De huidige context.
	 */
	private Context mContext;

	/**
	 * Niet-standaard constructor.
	 * 
	 * @param context De huidige context.
	 */
	public DownloadDataTask(Context context) {
		super();
		this.mContext = context;
	}

	/**
	 * Een array met parking objecten ophalen aan de hand van een gegeven url.
	 * 
	 * {@inheritDoc}
	 */
	@Override
	protected Parking[] doInBackground(String... urls) {

		// params comes from the execute() call: params[0] is the url.
		try {
			JSONObject JSONParkings = downloadUrl(urls[0]);
			JSONArray JSONArrayParkings = JSONParkings
					.getJSONObject("Parkings").getJSONArray("parkings");
			Parking[] parking = new Parking[JSONArrayParkings.length()];
			for (int i = 0; i < JSONArrayParkings.length(); i++) {
				JSONObject JSONParking = JSONArrayParkings.getJSONObject(i);
				String name = JSONParking.getString("name");
				int capacity = Integer.parseInt(JSONParking
						.getString("totalCapacity"));
				String available = JSONParking.getString("availableCapacity");
				int availableCapacity = 0;
				if (!available.equals("")) {
					availableCapacity = Integer.parseInt(available);
				}
				parking[i] = new Parking(name, capacity, availableCapacity);
			}
			return parking;
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * De listview en textViewStatus aanpassen aan de hand van het resultaat.
	 * 
	 * {@inheritDoc}
	 */
	@Override
	protected void onPostExecute(Parking[] result) {

		TextView textViewStatus = (TextView) ((Activity) this.mContext)
				.findViewById(R.id.textViewStatus);

		if (result != null) {
			ArrayAdapterParking adapter = new ArrayAdapterParking(
					this.mContext, R.layout.list_view_row_item, result);

			((ListView) ((Activity) this.mContext).findViewById(R.id.listview))
					.setAdapter(adapter);

			textViewStatus.setText("Updated: "
					+ DateFormat.getDateTimeInstance().format(new Date()));
			textViewStatus.setTextColor(Color.BLACK);
		} else {
			textViewStatus.setText("Error retrieving data.");
			textViewStatus.setTextColor(Color.RED);
		}
		((Activity) this.mContext).findViewById(R.id.buttonRefresh).setEnabled(
				true);
	}

	/**
	 * Aan de hand van een opgegeven url een json object ophalen.
	 * 
	 * @param myurl De opgegeven url waar het json object wordt opgehaald.
	 * @return Het json object
	 * @throws IOException
	 * @throws JSONException
	 */
	private JSONObject downloadUrl(String myurl) throws IOException,
			JSONException {
		InputStream is = null;

		try {
			URL url = new URL(myurl);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setReadTimeout(10000 /* milliseconds */);
			conn.setConnectTimeout(15000 /* milliseconds */);
			conn.setRequestMethod("GET");
			conn.setDoInput(true);
			conn.connect();
			int response = conn.getResponseCode();
			Log.d(DEBUG_TAG, "The response is: " + response);
			is = conn.getInputStream();

			JSONObject JSONParkings = readIt(is);
			return JSONParkings;

			// Makes sure that the InputStream is closed after the app is
			// finished using it.
		} finally {
			if (is != null) {
				is.close();
			}
		}
	}

	/**
	 * Een stream omzetten naar en json object.
	 * 
	 * @param stream De inputstream die wordt omgezet.
	 * @return Het resulterende json object.
	 * @throws IOException
	 * @throws UnsupportedEncodingException
	 * @throws JSONException
	 */
	public JSONObject readIt(InputStream stream) throws IOException,
			UnsupportedEncodingException, JSONException {
		BufferedReader r = new BufferedReader(new InputStreamReader(stream));
		StringBuilder total = new StringBuilder();
		String line;
		while ((line = r.readLine()) != null) {
			total.append(line);
		}

		JSONObject json = new JSONObject(total.toString());

		return json;
	}
}