/**
 * Deze klasse bevat gegevens over een specifieke parking.
 *
 * @author Geert Ari�n
 * @version 14 oktober 2014
 */

package com.example.androidclient;

public class Parking {

	/**
	 * De naam van de parking.
	 */
	private final String name;

	/**
	 * De capaciteit van de parking.
	 */
	private final int capacity;

	/**
	 * Het aantal vrije plaatsen van de parking.
	 */
	private int freePlaces;

	/**
	 * Naam van de parking ophalen.
	 * @return De naam van de parking.
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Capaciteit van de parking ophalen.
	 * @return De capaciteit van de parking.
	 */
	public int getCapacity() {
		return this.capacity;
	}

	/**
	 * Het aantal vrije plaatsen van de parking ophalen.
	 * @return Het aantal vrije plaatsen.
	 */
	public int getfreePlaces() {
		return this.freePlaces;
	}

	/**
	 * Het aantal vrije plaatsen van de parking wijzigen.
	 * @param freePlaces Het aantal vrije plaatsen.
	 */
	public void setOccupation(int freePlaces) {
		this.freePlaces = freePlaces;
	}

	/**
	 * Niet-standaard constructor.
	 * @param name De naam van de parking.
	 * @param capacity De capaciteit van de parking.
	 * @param freePlaces Het aantal vrije plaatsen van de parking.
	 */
	public Parking(String name, int capacity, int freePlaces) {
		this.name = name;
		this.capacity = capacity;
		this.freePlaces = freePlaces;
	}

}
